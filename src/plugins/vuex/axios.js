import axios from "axios";
// const axios = require('axios')
import store from "@/plugins/vuex/store";


axios.interceptors.request.use((config) => {
    if (config.url !== 'http://localhost:8080/api/users/auth') {
        config.headers['Content-Type'] = 'application/ld+json'
        config.headers['Authorization'] = 'bearer ' + store.getters.getToken
    }

    return config
})


export default axios