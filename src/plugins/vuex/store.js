import {createStore} from "vuex";
import registration from "@/plugins/modules/user/registration";
import authorization from "@/plugins/modules/user/authorization";
import addCategory from "@/plugins/modules/category/addCategory";
import getCategories from "@/plugins/modules/category/getCategories";
import changeCategory from "@/plugins/modules/category/changeCategory";
import removeCategory from "@/plugins/modules/category/removeCategory";
import createMediaObject from "@/plugins/modules/mediaObject/createMediaObject";
import createBook from "@/plugins/modules/book/createBook";
import getBooks from "@/plugins/modules/book/getBooks";
import getBook from "@/plugins/modules/book/getBook";

export default createStore({
    modules: {
        registration,
        authorization,
        addCategory,
        getCategories,
        changeCategory,
        removeCategory,
        createMediaObject,
        createBook,
        getBooks,
        getBook,
    }
})
