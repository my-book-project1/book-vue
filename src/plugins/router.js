import HomePage from "@/pages/HomePage.vue";
import {createRouter, createWebHistory} from "vue-router";
import store from "@/plugins/vuex/store";

const ifNotAuthorized = (to, from, next) => {
    if (!store.getters.getAuthorize) {
        next()
    } else {
        next('/')
    }
}

const ifAuthorized = (to, from, next) => {
    if (store.getters.getAuthorize) {
        next()
    } else {
        next('/log-in')
    }
}

const routes = [
    {
        path: '/',
        component: HomePage,
        beforeEnter: ifAuthorized
    },
    {
        path: '/categories/:id',
        component: HomePage,
        beforeEnter: ifAuthorized
    },
    {
        path: '/book-info/:foo',
        component: () =>
            import('../pages/BookInfoPage.vue'),
        beforeEnter: ifAuthorized
    },
    {
        path: '/log-in',
        name: 'LoginPage',
        component: () => import('../pages/LoginPage.vue'),
        meta: {layout: 'EntryLayout'},
        beforeEnter: ifNotAuthorized
    },
    {
        path: '/create-category',
        component: () =>
            import('../pages/Category/CreateCategory.vue'),
        beforeEnter: ifAuthorized
    },
    {
        path: '/edit-category',
        component: () =>
            import('../pages/Category/EditCategory.vue'),
        beforeEnter: ifAuthorized
    },
    {
        path: '/create-book',
        component: () =>
            import('../pages/book/CreateBook.vue'),
        beforeEnter: ifAuthorized
    },
]

export default createRouter({
    history: createWebHistory(),
    routes
})

