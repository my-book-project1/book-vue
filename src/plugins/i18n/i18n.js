import {createI18n} from "vue-i18n/dist/vue-i18n.esm-browser.prod";

import en from './locales/en.json'
import ru from './locales/ru.json'
import uz from './locales/uz.json'

const localeMessages = {
    en: en,
    ru: ru,
    uz: uz
}
// const messages = {}
// locales.keys().forEach(key => {
//
//
//     const matched = key.match(/([A-Za-z0-9-_]+)\./i)
//     if (matched && matched.length > 1) {
//         const locale = matched[1]
//         messages[locale] = locales(key)
//     }
// })
// return messages

export default createI18n({
    locale: 'en',
    fallbackLocale: 'uz',
    messages: localeMessages
})