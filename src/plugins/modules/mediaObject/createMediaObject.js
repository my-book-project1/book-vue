import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        pushMediaObject(context, data) {
            const file = new FormData()
            file.append('file', data)

            return new Promise((resolve, reject) => {
                axios.post('http://localhost:8080/api/media_objects', file)
                    .then((response) => {
                        console.log('Fayl yaratildi')

                        context.commit('updateFile', response.data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Fayl yaratishda xatolik yuz berdi')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateFile(state, data) {
            state.createdFile = data
        }
    },
    state: {
        createdFile: {}
    },
    getters: {
        getCreatedFile(state) {
            return state.createdFile
        }
    }
}