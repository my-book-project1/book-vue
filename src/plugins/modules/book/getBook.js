import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        fetchBook(context, id) {
            return new Promise((resolve, reject) => {
                axios.get('http://localhost:8080/api/books/' + id)
                    .then((response) => {
                        console.log('Kitob olindi')
                        console.log(response)

                        let book = {
                            "@id": response.data['@id'],
                            id: response.data.id,
                            name: response.data.name,
                            description: response.data.description,
                            text: response.data.text,
                            category: {
                                name: response.data.category.name
                            },
                            picture: {
                                contentUrl: response.data.picture.contentUrl
                            }
                        }

                        context.commit('updateBook', book)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Kitobni olishda xatolik')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateBook(state, data) {
            state.book = data
        }
    },
    state: {
        book: {
            "@id": null,
            id: null,
            name: null,
            description: null,
            text: null,
            category: {
                name:  null,
            },
            picture: {
                contentUrl: null,
            }
        }
    },
    getters: {
        getBook(state) {
            return state.book
        }
    }
}