import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        createBook(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('http://localhost:8080/api/books', data)
                    .then((response) => {
                        console.log("Kitob yaratildi")
                        console.log(response)

                        resolve()
                    })
                    .catch(() => {
                        console.log("Kitob yaratishda xatolik yuz berdi")
                        reject()
                    })
            })
        }
    },
}