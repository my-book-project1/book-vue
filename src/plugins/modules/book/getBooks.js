import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        fetchBooks(context, url=null) {
            let bookUrl = ''

            if (url) {
                bookUrl = url
            }
            return new Promise((resolve, reject) => {
                axios.get('http://localhost:8080/api/books' + bookUrl)
                    .then((response) => {
                        console.log('Kitoblar olindi')
                        console.log(response)

                        let books = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateBooks', books)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Kitoblarni olishda xatolik')
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateBooks(state, data) {
            state.books = data
        }
    },
    state: {
        books: {
            models: null,
            totalItems: 0,
        }
    },
    getters: {
        getBooks(state) {
            return state.books.models
        }
    }
}