import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        fetchCategories(context) {
            return new Promise((resolve, reject) => {
                axios.get('http://localhost:8080/api/categories')
                    .then((response) => {
                        console.log('Kategoriyalar olindi')
                        console.log(response)

                        let categories = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateCategories', categories)
                        resolve()
                    })
                    .catch((error) => {
                        console.log('Kategoriyalarni olishda xatolik')
                        console.log(error)
                        reject()
                    })
            })
        }
    },
    mutations: {
        updateCategories(state, data) {
            state.categories = data
        }
    },
    state: {
        categories: {
            models: null,
            totalItems: 0,
        }
    },
    getters: {
        getCategories(state) {
            return state.categories.models
        }
    }
}