import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        putCategory(context, data) {
            return new Promise((resolve, reject) => {
                axios.put('http://localhost:8080/api/categories/' + data.id, {name: data.name})
                    .then((response) => {
                        console.log('Kategoriya ozgartirildi')
                        console.log(response)

                        resolve()
                    })
                    .catch(() => {
                        console.log('Kategoriya ozgartirishda xatolik yuz berdi')
                        reject()

                    })
            })
        }
    }
}