import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        deleteCategory(context, id) {
            return new Promise((resolve, reject) => {
                axios.delete('http://localhost:8080/api/categories/' + id)
                    .then(() => {
                        console.log("Kategoriya o'chirildi")
                        resolve()
                    })
                    .catch(() => {
                        console.log("Kategoriya o'chrishda xatolik yuz berdi")
                        reject()
                    })
            })
        }
    }
}