import axios from "@/plugins/vuex/axios";

export default {
    actions: {
        createCategory(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('http://localhost:8080/api/categories', data)
                    .then((response) => {
                        console.log('Kategoriya yaratildi')
                        console.log(response)

                        resolve()
                    })
                    .catch((error) => {
                        console.log('Kategoriya yaratishda xatolik yuz berdi')
                        console.log(error)
                        reject()

                    })
            })
        }
    },
}