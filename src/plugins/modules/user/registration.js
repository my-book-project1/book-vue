import axios from "axios";

export default {
    actions: {
        createUser(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('http://localhost:8080/api/users', data)
                    .then((response) => {
                        console.log('Yangi user yaratildi')
                        console.log(response)

                        let data = {
                            id: response.data.id,
                            email: response.data.email,
                            age: response.data.age
                        }

                        context.commit('updateCreatedUser', data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('User yaratishda xatolik yuz berdi')
                        reject()
                    })
                    .finally(() => {
                        console.log('Finally doim ishlaydi')
                    })
            })
        }
    },
    mutations: {
        updateCreatedUser(state, data) {
            state.createdUser = data
        }
    },
    state: {
        createUser: {
            id: null,
            email: null,
            age: null
        }
    },
    getters: {
        getCreatedUser(state) {
            return state.createdUser
        }
    },
}
